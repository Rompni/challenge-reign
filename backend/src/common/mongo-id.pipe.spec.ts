import { MongoIdPipe } from './mongo-id.pipe';
import { isMongoId } from 'class-validator';
import { ArgumentMetadata } from '@nestjs/common';

const mockValidId = '60c920f559884348f23ee5b2';
const mockInvalidId = '1aa11';

describe('MongoIdPipe Validation', () => {
  let target: MongoIdPipe;

  it('should be defined', () => {
    expect(new MongoIdPipe()).toBeDefined();
  });

  describe('when the validation passes', () => {
    target = new MongoIdPipe();

    it('should be a valid mongoId', () => {
      expect(isMongoId(mockValidId)).toBeTruthy();
    });

    it('should return string if value is MongoId', async () => {
      expect(
        await target.transform(mockValidId, {} as ArgumentMetadata),
      ).toEqual(mockValidId);
    });
  });

  describe('when the validation fails', () => {
    test('should throw an error - 1', async () => {
      target = new MongoIdPipe();
      expect(() => target.transform('123a', {} as ArgumentMetadata)).toThrow(
        `123a is not a mongoID`,
      );
    });

    test('should throw an error - 2', async () => {
      target = new MongoIdPipe();

      expect(() =>
        target.transform(mockInvalidId, {} as ArgumentMetadata),
      ).toThrow(`${mockInvalidId} is not a mongoID`);
    });
  });
});
