import { Test, TestingModule } from '@nestjs/testing';
import { ArticlesController } from './articles.controller';
import { ArticlesService } from '../services/articles.service';
import { CreateArticleDto, UpdateArticleDto } from '../dto/create-article.dto';
import { Article } from '../entities/article.entity';

const today = new Date();

const mockArticle1: CreateArticleDto = {
  author: 'rompni',
  created_at: new Date(),
  created_at_i: today.getTime(),
  objectID: '1188444',
  deleted: false,
};

const mockArticleDoc1 = (
  mock?: Partial<CreateArticleDto>,
): Partial<Article> => ({
  author: mock?.author || 'rompni',
  created_at: mock?.created_at || new Date(),
  created_at_i: mock?.created_at_i || today.getTime(),
  objectID: mock?.objectID || '1188444',
  deleted: mock?.deleted || false,
  _id: '1213123122',
});

const mockArticleUpdated = {
  author: 'rompni',
  created_at: mockArticle1.created_at,
  created_at_i: mockArticle1.created_at_i,
  objectID: mockArticle1.objectID,
  deleted: true,
  _id: '1213123122',
};

const mockArrayArticles: CreateArticleDto[] = [mockArticle1, mockArticle1];

const mockArrayDocArticles = [mockArticleDoc1, mockArticleDoc1];

const mockArticlesService = {
  create: jest.fn((dto) => {
    return {
      _id: Date.now().toString(),
      ...dto,
    };
  }),
  update: jest.fn().mockImplementation((id, dto: UpdateArticleDto) => {
    return mockArticleUpdated;
  }),
  remove: jest.fn().mockImplementation((id) => {
    return mockArticleDoc1;
  }),
  findOne: jest.fn().mockImplementation((id) => {
    return mockArticleDoc1;
  }),
  findAll: jest.fn(() => {
    return [mockArticleDoc1, mockArticleDoc1];
  }),
};

describe('ArticlesController', () => {
  let controller: ArticlesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [ArticlesService],
      controllers: [ArticlesController],
      providers: [ArticlesService],
    })
      .overrideProvider(ArticlesService)
      .useValue(mockArticlesService)
      .compile();

    controller = module.get<ArticlesController>(ArticlesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should create an article', () => {
    expect(controller.create(mockArticle1)).toEqual({
      _id: expect.any(String),
      ...mockArticle1,
    });

    expect(mockArticlesService.create).toHaveBeenCalledWith(mockArticle1);
  });

  it('should update an article', () => {
    expect(controller.update('123213123')).toEqual({
      ...mockArticleUpdated,
    });

    expect(mockArticlesService.update).toHaveBeenCalled();
  });

  it('should delete an article', () => {
    expect(controller.remove('1213123122')).toEqual(mockArticleDoc1);

    expect(mockArticlesService.remove).toHaveBeenCalled();
  });

  it('should find an article', () => {
    expect(controller.findOne('1213123122')).toEqual(mockArticleDoc1);

    expect(mockArticlesService.findOne).toHaveBeenCalled();
  });
});
