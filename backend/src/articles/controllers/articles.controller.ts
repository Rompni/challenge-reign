import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { ArticlesService } from '../services/articles.service';
import { CreateArticleDto } from '../dto/create-article.dto';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { MongoIdPipe } from '../../common/mongo-id.pipe';
import { Article } from '../entities/article.entity';

@ApiTags('Articles')
@Controller('articles')
export class ArticlesController {
  constructor(private articlesService: ArticlesService) {}

  @Post()
  @ApiOperation({ summary: 'Create a new article' })
  create(@Body() createArticleDto: CreateArticleDto) {
    return this.articlesService.create(createArticleDto);
  }

  @Get()
  @ApiOperation({ summary: 'Return all articles' })
  async findAll(): Promise<Article[]> {
    return await this.articlesService.findAll();
  }

  @Get(':id')
  @ApiOperation({ summary: 'Return an article' })
  findOne(@Param('id', MongoIdPipe) id: string) {
    return this.articlesService.findOne(id);
  }

  @Patch(':id')
  @ApiOperation({ summary: 'Update an article' })
  update(@Param('id', MongoIdPipe) id: string) {
    return this.articlesService.update(id, { deleted: true });
  }

  @Delete(':id')
  @ApiOperation({ summary: 'Delete an article' })
  remove(@Param('id', MongoIdPipe) id: string) {
    return this.articlesService.remove(id);
  }
}
