import {
  IsArray,
  IsBoolean,
  IsDate,
  IsEmpty,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsPositive,
  IsString,
  IsUrl,
} from 'class-validator';
import { ApiProperty, PartialType } from '@nestjs/swagger';
import { Type } from 'class-transformer';

export class CreateArticleDto {
  @IsDate()
  @IsNotEmpty()
  @ApiProperty()
  readonly created_at: Date;

  @IsString()
  @IsOptional()
  @ApiProperty()
  readonly title?: string;

  @IsString()
  @IsUrl()
  @IsOptional()
  @ApiProperty()
  readonly url?: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  readonly author: string;

  @IsNumber()
  @IsPositive()
  @IsOptional()
  @ApiProperty()
  readonly points?: number;

  @IsString()
  @IsOptional()
  @ApiProperty()
  readonly story_text?: string;

  @IsString()
  @IsOptional()
  @ApiProperty()
  readonly comment_text?: string;

  @IsNumber()
  @IsOptional()
  @ApiProperty()
  readonly num_comments?: number;

  @IsNumber()
  @IsOptional()
  @ApiProperty()
  story_id?: number;

  @IsString()
  @IsOptional()
  @ApiProperty()
  story_title?: string;

  @IsNumber()
  @IsOptional()
  @ApiProperty()
  readonly story_url?: string;

  @IsNumber()
  @IsOptional()
  @ApiProperty()
  readonly parent_id?: number;

  @IsNumber()
  @IsNotEmpty()
  @ApiProperty()
  readonly created_at_i: number;

  @IsArray()
  @IsOptional()
  @ApiProperty()
  readonly _tags?: string[];

  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  readonly objectID: string;

  @IsBoolean()
  @IsOptional()
  @ApiProperty()
  readonly deleted?: boolean;
}

export class UpdateArticleDto extends PartialType(CreateArticleDto) {}
