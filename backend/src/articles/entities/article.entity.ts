import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema()
export class Article extends Document {
  @Prop({ required: true })
  created_at: Date;

  @Prop({ required: false })
  title?: string;

  @Prop({ required: false })
  url?: string;

  @Prop({ required: true })
  author: string;

  @Prop({ type: Number, required: false })
  points?: number;

  @Prop({ required: false })
  story_text?: string;

  @Prop({ required: false })
  comment_text?: string;

  @Prop({ type: Number })
  num_comments?: number;

  @Prop({ required: false, type: Number })
  story_id: number;

  @Prop({ required: false })
  story_title?: string;

  @Prop({ required: false })
  story_url?: string;

  @Prop({ type: Number })
  parent_id?: number;

  @Prop({ required: true, type: Number })
  created_at_i: number;

  @Prop({ required: false })
  _tags?: string[];

  @Prop({ required: true })
  objectID?: string;

  @Prop({ default: false, required: false })
  deleted?: boolean;
}

export const ArticleSchema = SchemaFactory.createForClass(Article);
