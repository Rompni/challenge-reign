import { Test, TestingModule } from '@nestjs/testing';
import { ArticlesService } from './articles.service';
import { Article } from '../entities/article.entity';
import { Model } from 'mongoose';
import { getModelToken } from '@nestjs/mongoose';
import { CreateArticleDto } from '../dto/create-article.dto';
import { ArticlesController } from '../controllers/articles.controller';

const today = new Date();

const mockArticle = (
  created_at = today,
  title = null,
  url = null,
  author = 'dreamer7',
  points = null,
  story_text = null,
  comment_text = 'Recently I started incorporating n8n',
  num_comments = null,
  story_id = 27516212,
  story_title = 'Utopia, a visual design tool for React, with code as the source of truth',
  story_url = 'https://utopia.app/',
  parent_id = 27522664,
  created_at_i = 1623820179,
  _tags = ['comment', 'author_dreamer7', 'story_27516212'],
  objectID = '27525118',
): {
  comment_text: string;
  story_text: any;
  author: string;
  story_id: number;
  _tags: string[];
  created_at: Date;
  created_at_i: number;
  title: any;
  url: any;
  points: any;
  num_comments: any;
  story_title: string;
  parent_id: number;
  story_url: string;
  objectID: string;
} => ({
  created_at,
  title,
  url,
  author,
  points,
  story_text,
  comment_text,
  num_comments,
  story_title,
  story_url,
  parent_id,
  created_at_i,
  _tags,
  objectID,
  story_id,
});

const mockArticleDoc = (
  mock?: Partial<CreateArticleDto>,
): Partial<Article> => ({
  comment_text: mock?.comment_text || 'Recently I started incorporating n8n',
  story_text: mock?.story_text || null,
  author: mock?.author || 'dreamer7',
  story_id: mock?.story_id || 27516212,
  _tags: mock?._tags || ['comment', 'author_dreamer7', 'story_27516212'],
  created_at: mock?.created_at || today,
  created_at_i: mock?.created_at_i || 1623820179,
  title: mock?.title || null,
  url: mock?.url || null,
  points: mock?.points || null,
  num_comments: mock?.points || null,
  story_title:
    mock?.story_title ||
    'Utopia, a visual design tool for React, with code as the source of truth',
  parent_id: mock?.parent_id || 27522664,
  story_url: mock?.story_url || 'https://utopia.app/',
  objectID: mock?.objectID || '27525118',
});

const articleArray: CreateArticleDto[] = [mockArticle(), mockArticle()];

const articleArrayDoc = [mockArticleDoc(), mockArticleDoc()];

const mockArticleService = {};

const mockArticleModel = {
  provide: getModelToken('Article'),
  useValue: {
    new: jest.fn().mockResolvedValue(mockArticle()),
    constructor: jest.fn().mockResolvedValue(mockArticle()),
    find: jest.fn(),
    findOne: jest.fn(),
    update: jest.fn(),
    create: jest.fn(),
    remove: jest.fn(),
    exec: jest.fn(),
    findById: jest.fn(),
  },
};

describe('ArticlesService', () => {
  let service: ArticlesService;
  let model: Model<Article>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ArticlesService, mockArticleModel],
    }).compile();

    service = module.get<ArticlesService>(ArticlesService);
    model = module.get<Model<Article>>(getModelToken('Article'));
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should return all articles', async () => {
    jest.spyOn(model, 'find').mockReturnValue({
      exec: jest.fn().mockResolvedValueOnce(articleArrayDoc),
    } as any);

    const articles = await service.findAll();
    expect(articles).toEqual(articleArray);
  });

  it('should return an articles', async () => {
    jest.spyOn(model, 'findOne').mockReturnValue({
      exec: jest.fn().mockResolvedValueOnce(mockArticleDoc),
    } as any);
  });
});
