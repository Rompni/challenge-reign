import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateArticleDto, UpdateArticleDto } from '../dto/create-article.dto';
import { InjectModel } from '@nestjs/mongoose';
import { Article } from '../entities/article.entity';
import { Model } from 'mongoose';

@Injectable()
export class ArticlesService {
  constructor(
    @InjectModel(Article.name) private articleModel: Model<Article>,
  ) {}
  async create(createArticleDto: CreateArticleDto) {
    this.articleModel
      .find({
        objectID: createArticleDto['objectID'],
      })
      .exec()
      .then(async (r) => {
        if (r.length <= 0) {
          const newArticle = await new this.articleModel(createArticleDto);
          return newArticle.save();
        } else {
          return;
        }
      });
  }

  async findAll() {
    return await this.articleModel.find({ deleted: false }).exec();
  }

  async findOne(id: string) {
    const article = await this.articleModel.findById(id);
    if (!article) {
      throw new NotFoundException(`Article #${id} not found`);
    }
    return article;
  }

  async update(id: string, updateArticleDto: UpdateArticleDto) {
    const article = await this.articleModel
      .findByIdAndUpdate(
        id,
        {
          $set: updateArticleDto,
        },
        { new: true },
      )
      .exec();

    if (!article) {
      throw new NotFoundException(`Article #${id} not found`);
    }

    return article;
  }

  async remove(id: string) {
    const article = await this.articleModel.findByIdAndRemove(id);

    if (!article) {
      throw new NotFoundException(`Article #${id} not found`);
    }

    return article;
  }
}
