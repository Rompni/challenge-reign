import { Inject, Injectable, Logger, OnModuleInit } from '@nestjs/common';
import { ArticlesService } from './articles/services/articles.service';

@Injectable()
export class AppService implements OnModuleInit {
  constructor(
    @Inject('ARTICLES') private articles: any[],
    private articleService: ArticlesService,
  ) {}

  onModuleInit(): any {
    Logger.log('Creating Database', 'Database');
    const data = [...this.articles['hits']];
    try {
      data.map(async (item) => {
        await this.articleService.create(item);
      });
    } catch (e) {
      Logger.error('Error creating data', 'Database');
    }
    Logger.log('Creation Completed', 'Database');
  }
}
