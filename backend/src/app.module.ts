import { HttpModule, HttpService, Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { DatabaseModule } from './database/database.module';
import { appDB } from '../config';
import { MongooseModule } from '@nestjs/mongoose';
import { ArticlesModule } from './articles/articles.module';

const { connection, port, host, dbName } = appDB;

@Module({
  imports: [
    MongooseModule.forRoot(
      `${connection}://${host}:${port}/${dbName}?authSource=admin`,
    ),
    HttpModule,
    DatabaseModule,
    ArticlesModule,
  ],
  controllers: [AppController],
  providers: [
    AppService,
    {
      provide: 'ARTICLES',
      useFactory: async (http: HttpService) => {
        const articles = await http
          .get('https://hn.algolia.com/api/v1/search_by_date?query=nodejs')
          .toPromise();

        return articles.data;
      },
      inject: [HttpService],
    },
  ],
})
export class AppModule {}
