import { Global, Logger, Module } from '@nestjs/common';
import { MongoClient } from 'mongodb';
import { appDB } from '../../config';

@Global()
@Module({
  providers: [
    {
      provide: 'MONGO',
      useFactory: async () => {
        try {
          const { connection, port, host, dbName } = appDB;
          const uri = `${connection}://${host}:${port}?authSource=admin/`;
          Logger.log(`Trying to connect to: ${uri}`, 'Database');
          const client = new MongoClient(uri);
          await client.connect().then((r) => {
            Logger.log(`Connected Database`, 'Database');
          });
          return client.db(dbName);
        } catch (e) {
          Logger.log('Error to Connect ' + e.message, 'Database');
        }
      },
    },
  ],
  exports: ['MONGO'],
})
export class DatabaseModule {}
