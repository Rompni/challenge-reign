import * as dotenv from 'dotenv';
import { resolve } from 'path';

const envFileName =
  process.env.NODE_ENV === 'test' ? '../.env.test' : '../.env';

dotenv.config({ path: resolve(__dirname, envFileName) });

export const PORT = process.env.PORT || 3000;

export const appDB = {
  dbName: process.env.MONGO_DB.toString(),
  port: parseInt(process.env.MONGO_PORT, 10),
  host: process.env.MONGO_HOST.toString(),
  connection: process.env.MONGO_CONNECTION.toString(),
};
