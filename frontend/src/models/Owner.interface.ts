export interface IOwner {
  story_url: string;
  url?: string | null;
  title?: string | null;
  story_title?: string | null;
  author: string;
}
