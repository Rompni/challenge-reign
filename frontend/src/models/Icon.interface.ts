export interface ITrashIcon {
  _id: string;
  onDelete?: () => void;
}
