export interface IArticle {
  _id: string;
  created_at: string;
  title?: string | null;
  url?: string | null;
  author: string;
  points?: number | null;
  story_text?: string | null;
  comment_text?: string | null;
  num_comments?: number | null;
  story_id: number;
  story_title?: string | null;
  story_url: string;
  parent_id?: number | null;
  created_at_i: number;
  _tags?: string[] | null;
  objectID: string;
  deleted?: boolean | null;
}
