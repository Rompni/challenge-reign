import axios, { AxiosResponse } from "axios";
import { IArticle } from "../models/Article.interface";

const instance = axios.create({
  baseURL: process.env.REACT_APP_API,
  timeout: 15000,
});

const responseBody = (response: AxiosResponse) => response.data;
const requests = {
  get: (url: string) => instance.get(url).then(responseBody),
  post: (url: string, body: {}) => instance.post(url, body).then(responseBody),
  put: (url: string) => instance.patch(url).then(responseBody),
  delete: (url: string) => instance.delete(url).then(responseBody),
};

export const Articles = {
  getArticles: (): Promise<IArticle[]> => requests.get("articles"),
  getAnArticle: (id: number): Promise<IArticle> =>
    requests.get(`articles/${id}`),
  updateArticle: (id: string): Promise<IArticle> =>
    requests.put(`articles/${id}`),
};
