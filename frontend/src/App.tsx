import React, { useEffect, useState } from "react";
import "./App.css";
import { IArticle } from "./models/Article.interface";
import { Articles } from "./api/api";
import Banner from "./components/Banner";
import Owner from "./components/Owner";
import TrashIcon from "./components/TrashIcon";
import MyDate from "./components/MyDate";

function App() {
  const [articles, setArticles] = useState<IArticle[]>([]);
  const [isError, setIsError] = useState<boolean>(false);

  useEffect(() => {
    Articles.getArticles()
      .then((data) => {
        setArticles(
          data.sort((a, b) => {
            return b.created_at_i - a.created_at_i;
          })
        );
      })
      .catch((err) => {
        setIsError(true);
      });
    return () => {};
  }, []);

  const handleOnDelete = (_id: string) => {
    const data = articles.filter((article) => article._id !== _id);
    setArticles(data);
  };

  return (
    <div>
      <Banner />
      {isError ? <h1 className="title no-data"> No Data</h1> :
          <div className="container">
            {articles.map(
                (
                    {
                      _id,
                      story_title,
                      title,
                      url,
                      story_url,
                      author,
                      created_at,
                      objectID,
                    },
                    i
                ) =>
                    (story_title || title) && (
                        <div key={i} className="row-normal row">
                          <Owner
                              story_url={story_url}
                              url={url}
                              title={title}
                              story_title={story_title}
                              author={author}
                          />

                          <MyDate created_at={created_at}/>

                          <TrashIcon
                              _id={_id}
                              onDelete={() => {
                                handleOnDelete(_id);
                              }}
                          />
                        </div>
                    )
            )}
          </div>
      }
    </div>
  );
}

export default App;
