import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTrash } from "@fortawesome/free-solid-svg-icons";
import Swal from "sweetalert2";
import { Articles } from "../api/api";
import { ITrashIcon } from "../models/Icon.interface";

const TrashIcon = ({ _id, onDelete }: ITrashIcon): JSX.Element => {
  const handleDelete = (
    objectID: string,
    e: React.MouseEvent<HTMLDivElement>
  ) => {
    Swal.fire({
      title: "Are you sure?",
      text: "You won't be able to revert this!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!",
    }).then((result) => {
      if (result.isConfirmed) {
        if (onDelete) {
          onDelete();
        }

        Articles.updateArticle(objectID)
          .then((r) => {
            Swal.fire("Deleted!", "Your file has been deleted.", "success");
          })
          .catch(() =>
            Swal.fire("Error!", "Error, there is a problem.", "error")
          );
      }
    });
  };

  return (
    <div className="trash" onClick={(e) => handleDelete(_id, e)}>
      <span>
        <FontAwesomeIcon icon={faTrash} />
      </span>
    </div>
  );
};

export default TrashIcon;
