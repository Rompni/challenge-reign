import React from "react";
import { IOwner } from "../models/Owner.interface";

const Owner = ({
  url = "",
  story_url,
  story_title,
  title,
  author,
}: IOwner): JSX.Element => {
  return (
    <div>
      <a href={(story_url ? story_url : url) || ""}>
        <span className="title">{story_title ? story_title : title}</span>
        <span className="author">- {author} -</span>
      </a>
    </div>
  );
};

export default Owner;
