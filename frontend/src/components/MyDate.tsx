import React from "react";

interface IMyDate {
  created_at: string;
}
const MyDate = ({ created_at }: IMyDate): JSX.Element => {
  const today = new Date();

  const formatDate = (date: Date): string => {
    const day = date.getDay();
    const month = date.getMonth();
    const year = date.getFullYear();
    if (
      day === today.getDay() &&
      month === today.getMonth() &&
      year === today.getFullYear()
    ) {

      return date.toLocaleString('en', {hour: "2-digit", minute: "2-digit"});

    } else if (
      day === today.getDay() - 1 &&
      month === today.getMonth() &&
      year === today.getFullYear()
    ) {

      return "Yesterday "
    }

    return date.toLocaleString('en', {month: 'short', day: "2-digit"});
  };

  return (
    <div>
      <span className="time">{formatDate(new Date(created_at))}</span>
    </div>
  );
};

export default MyDate;
