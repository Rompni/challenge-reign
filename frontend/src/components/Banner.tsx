import React from "react";

const Banner = (): JSX.Element => {
  return (
    <div>
      <div className="banner">
        <h1>HN Feed</h1>
        <h2>{`We <3 Hacker News`}</h2>
      </div>
    </div>
  );
};

export default Banner;
