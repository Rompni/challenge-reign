# challenge-reign

To run the project in, you must first create the environment variables '.env' on the client and on the server.
in each project there is an example of how to configure your environment variable.

*client .env:*

    REACT_APP_API=http://localhost:5000
    SKIP_PREFLIGHT_CHECK=true
    

*server .env:*

    DATABASE_NAME=articles
    PORT=5000
    MONGO_DB=challenge
    MONGO_PORT=27017
    MONGO_HOST=localhost
    MONGO_CONNECTION=mongodb
    

*server .env.docker:*

    DATABASE_NAME=articles
    PORT=5000
    MONGO_DB=challenge
    MONGO_PORT=27017
    MONGO_HOST=mongo
    MONGO_CONNECTION=mongodb


then being in the main folder:

We use the command `docker-compose up` the project will start up.
